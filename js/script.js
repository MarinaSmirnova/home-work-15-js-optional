// "Добуток всіх натуральних чисел від 1 до n називається факторіалом числа n."

function takeNumberArgument(text) { 
    let value;
    let defaultValue = undefined;

    do {
        value = prompt(text, defaultValue);
        defaultValue = value;
        value = Number(value);

    } while ((isNaN(value)) || (value <= 0));

    return value;
}




// Варіант без рекурсії

function factorial(n) {
    let factorial = 1;
    for (let i = 1; i <= n; i++) {
        factorial *= i;
    }
    return factorial;
}

let number = takeNumberArgument("Write the number");

console.log(factorial(number));




// Варіант з рекурсією

// function factorial(n) {
//     if (n == 1) {
//         return 1;
//     } else {
//         return n * factorial(n - 1);
//     } 
// }

// let number = takeNumberArgument("Write the number");

// console.log(factorial(number));
